package observability.test.sajal.monitoringsystem.temp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.Map;

@RestController
@RequestMapping("/names")
@RequiredArgsConstructor
@Slf4j
public class MyController {
    private final MyService myService;
    @GetMapping
    public Flux<Map<String, String>> getAllEmployees() {
        return myService
                .getValues()
                .doOnRequest(l -> log.info("Request received"))
                .doOnComplete(() -> log.info("Request served successfully"))
                ;
    }
}
