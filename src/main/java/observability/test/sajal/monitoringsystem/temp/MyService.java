package observability.test.sajal.monitoringsystem.temp;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.random.RandomGenerator;

@Service
@RequiredArgsConstructor
@Slf4j
public class MyService {
    private static final Map<String, String> values = new HashMap<>();
    private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final Random RANDOM = new Random();

    public Flux<Map<String, String>> getValues() {
        return Flux
                .range(1,10)
                .doOnRequest(l -> log.info("Starting the generation process of making random string values"))
                .flatMap(i-> Mono.fromCallable(() -> {
                    values.put(String.valueOf(i),generateRandomString(10));
                    return values;
                }))
                .doOnSubscribe(subscription -> log.info("Subscribed to the generation process: - {}", subscription.toString()))
                .doOnComplete(() -> log.info("Finished generation process"))
                .doOnError(throwable -> {
                    log.error("Error occurred while generating random string values", throwable);
                    throw new RuntimeException("Error occurred while generating random string values", throwable);
                })
                ;
    }




    private String generateRandomString(int length) {
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(CHARACTERS.charAt(RANDOM.nextInt(CHARACTERS.length())));
        }
        return sb.toString();
    }
}
